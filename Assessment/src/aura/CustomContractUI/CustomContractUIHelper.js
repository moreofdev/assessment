({
	getRecords : function(component, event) {
		var action = component.get("c.getContracts");
        action.setParams({
        recordid : component.get("v.recordId")
           
    });

        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if(state === "SUCCESS")
            {
                component.set("v.Contracts", actionResult.getReturnValue());
                
            }
            else if(state === "ERROR"){
                $A.log("ERRORS", a.getError());
            }
        });
        $A.enqueueAction(action);
	}
})
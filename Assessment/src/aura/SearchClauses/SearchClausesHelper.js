({
    searchClauses : function(component,event,helper) {
        
        var action = component.get("c.getSearchResults");
        action.setParams({
            'SearchTerm': component.get("v.searchKeyword"),
            'recordId': component.get("v.recordId")
        });
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
               
               //  if storeResponse size is 0 ,display no record found message on screen.
                if (storeResponse.length == 0) {
                    component.set("v.Message", true);
                    component.set("v.addBtn",false);
                } else {
                    component.set("v.Message", false);
                    component.set("v.addBtn",true);
                }
                
                // set searchResult list with return value from server.
                component.set("v.searchResult", storeResponse); 
              
                
            }else if (state === "INCOMPLETE") {
                alert('Response is Incompleted');
                component.set("v.addBtn",false);
            }else if (state === "ERROR") {
                component.set("v.addBtn",false);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        alert("Error message: " + 
                              errors[0].message);
                    }
                } else {
                    alert("Unknown error");
                    component.set("v.addBtn",false);
                }
            }
        });
        $A.enqueueAction(action);
        
        
        
    },
    
    addSelectedClauses: function(component, event, tempIDs){
	     
        var action = component.get("c.createParentRecords");
        action.setParams({
            "parentId" : component.get("v.recordId"),
            "listOfClauseIds": tempIDs
        });
        action.setCallback(this, function(response) {
           
            var state = response.getState();
          
            if (state === "SUCCESS") 
            { 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The Child record's has been added successfully."
                });
                toastEvent.fire();
                
                // refresh/reload the page view
                $A.get('e.force:navigateToSObject').setParams({
                    "recordId":component.get("v.recordId")
                }).fire();
                
                // call init function again [clear selected checkboxes]
                this.searchClauses(component,event);
                
            }
            
        });
        $A.enqueueAction(action);
    }
    
})
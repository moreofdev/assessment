({
	Search : function(component, event, helper) {
		
        var searchField = component.find('searchField');
        var isValueMissing = searchField.get('v.validity').valueMissing;
        // if value is missing show error message and focus on field
        if(isValueMissing){
            searchField.showHelpMessageIfInvalid();
            searchField.focus();
            component.set("v.addBtn",false);
        }else{
          // else call helper function 
            helper.searchClauses(component, event);
        }
        
        
	},
  
    addSelected : function(component, event, helper){
		var tempIDs = [];
        // get(find) all checkboxes with aura:id "checkBox"
        var getAllId = component.find("checkBox");
       
       
        // if value is checked(true) then add those Id (store in Text attribute on checkbox) in tempIDs var.
        for (var i = 0; i < getAllId.length; i++) {
            if (getAllId[i].get("v.value") == true) {
                tempIDs.push(getAllId[i].get("v.text"));
            }
        }      
        if(tempIDs.length >0){
             // call the helper function and pass all selected record id's.   
       		 helper.addSelectedClauses(component, event, tempIDs);
        }       
        
	}
})
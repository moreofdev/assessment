({
	getClauses : function(component,event) {
		var action = component.get("c.getRelatedClauses");
       
       action.setParams({
        "recordid" : component.get("v.recordId")
           
    });
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            var returnedList = actionResult.getReturnValue();
            var listLength = returnedList.length;
          
            if(state === "SUCCESS")
            {
      
                if(listLength>0)
                {  
                   component.set("v.relatedClauses", returnedList);
                   component.set("v.removeBtn", true); 
                }
                else{
                    component.set("v.Message", true);
                    component.set("v.removeBtn", false);
                }
               
                
            }
            else if(state === "ERROR"){
               
                $A.log("ERRORS", a.getError());
            }
        });
        $A.enqueueAction(action);
	},
    
   
    /*************************************************************************************************/
    removeSelectedClauses: function(component,event,tempIDs){
        var action = component.get("c.removeClausesFromContract");

        action.setParams({
            "clauseList": tempIDs
        });
        action.setCallback(this, function(response) {
            var state = response.getState();
        
            if (state === "SUCCESS") 
            { 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The Child record's has been removed successfully."
                });
                toastEvent.fire();
                
                // refresh/reload the page view
                $A.get('e.force:navigateToSObject').setParams({
                    "recordId":component.get("v.recordId")
                }).fire();
                
               
            }
            
        });
        $A.enqueueAction(action);
    }
})
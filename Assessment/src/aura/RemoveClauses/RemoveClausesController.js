({
    doInit : function(component, event, helper) {
        helper.getClauses(component,event,helper);
    },
    
    
    removeSelected: function(component,event,helper){
        
        var tempIDs = [];
        // get(find) all checkboxes with aura:id "checkBox"
        var getAllId = component.find("checkBox");
        
        // if value is checked(true) then add those Id (store in Text attribute on checkbox) in tempIDs var.
        for (var i = 0; i < getAllId.length; i++) {
            if (getAllId[i].get("v.value") == true && getAllId[i] != 0) {
                tempIDs.push(getAllId[i].get("v.text"));
            }
        } 
        
        helper.removeSelectedClauses(component, event, tempIDs);
    },
    
})
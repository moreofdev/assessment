({
	getClauses : function(component,event,helper) {
		var action = component.get("c.getAllClauses");
   
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if(state === "SUCCESS")
            {
                component.set("v.Clauses", actionResult.getReturnValue());
                                  
            }
            else if(state === "ERROR"){
               
                $A.log("ERRORS", a.getError());
            }
        });
        $A.enqueueAction(action);
	},
    
    getRelatedClauses : function(component,event,helper) {
		var action = component.get("c.getRelatedClauses");
        
       action.setParams({
        "recordid" : component.get("v.recordId")
           
    });
   
        action.setCallback(this, function(actionResult){
            var state = actionResult.getState();
            if(state === "SUCCESS")
            {
                if(actionResult.getReturnValue().length >0)
                {
                     component.set("v.Clauses", actionResult.getReturnValue());
                }
                else{
                    component.set("v.Message", true);
                }

            }
            else if(state === "ERROR"){
               
                $A.log("ERRORS", a.getError());
            }
        });
        $A.enqueueAction(action);
	}
    
})
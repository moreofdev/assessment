({
	doInit : function(component, event, helper) {
		helper.getRelatedClauses(component, event);
	},
    
    openAddModal : function(component,event,helper){
    
       component.set("v.isOpenAdd", true);
	},
    closeAddModal: function(component,event,helper){
       component.set("v.isOpenAdd", false);
    },
    openRemoveModal : function(component,event,helper){
    	component.set("v.isOpenRemove", true);
	},
    closeRemoveModal : function(component,event){
     	  component.set("v.isOpenRemove", false);
    }
    
    
})
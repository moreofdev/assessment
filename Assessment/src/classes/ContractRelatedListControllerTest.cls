@isTest
public class ContractRelatedListControllerTest {

    private static testMethod void getAllClauses(){
  	
       test.startTest();
      for(integer i=0;i<10;i++)
     {
        Clause__c cl = new Clause__c();
        cl.Name = 'test'+i;
        cl.Description__c = 'test description'+i;
        cl.Type__c = 'Financial';
        insert cl;
     }
       
        
        List<Clause__c> numCl = new List<Clause__c>();
        
        numCl = ContractRelatedListController.getAllClauses();
        test.stopTest();
      
        System.assertEquals(10, numCl.size());
        
     
    }
    private static testmethod void getRelatedClauses()
    {
     	test.startTest();   
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contract ct = new Contract();
        ct.AccountId= acc.Id;
        ct.Status='Draft';
        ct.ContractTerm = 20;
        insert ct;
        
         Clause__c cl1 = new Clause__c();
            cl1.Name = 'test1';
            cl1.Description__c = 'test description';
            cl1.Type__c = 'Financial'; 
             
            Clause__c cl2 = new Clause__c();
            cl2.Name = 'test1';
            cl2.Description__c = 'test description';
            cl2.Type__c = 'Other';
       		
            insert cl1;
        	insert cl2;
        
        for(integer i=0;i<2;i++)
        {
            Contract_Clause__c ccl = new Contract_Clause__c();
        	ccl.Clause__c = cl1.Id;
        	ccl.Contract__c = ct.Id;
        
       		Contract_Clause__c ccl1 = new Contract_Clause__c();
        	ccl1.Clause__c = cl2.Id;
        	ccl1.Contract__c = ct.Id;
            
            insert ccl;
          	insert ccl1;
        }
        
       		List<Contract_Clause__c> ccList = ContractRelatedListController.getRelatedClauses(ct.Id);  
       		test.stopTest();	
        
        system.assertEquals(4, ccList.size());
        
          
    }
    
}
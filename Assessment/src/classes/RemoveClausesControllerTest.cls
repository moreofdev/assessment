@isTest
public class RemoveClausesControllerTest {
   
    private static testMethod void getRelatedClauses()
    {
        test.startTest();
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contract ct = new Contract();
        ct.AccountId= acc.Id;
        ct.Status='Draft';
        ct.ContractTerm = 20;
        insert ct;
        
        Clause__c cl = new Clause__c();
        cl.Name = 'test';
        cl.Description__c = 'test description';
        cl.Type__c = 'Financial';
        
        insert cl;
        
        Contract_Clause__c ccl = new Contract_Clause__c();
        ccl.Clause__c = cl.Id;
        ccl.Contract__c = ct.Id;
        
        insert ccl;
        
        List<Contract_Clause__c> clist = new List<Contract_Clause__c>();
        clist = RemoveClausesController.getRelatedClauses(ct.Id);
        test.stopTest();
        
        system.assertEquals(clist.size(), 1);
        
    }   
    
    private static testMethod void removeClausesFromContract()
    {
        test.startTest();
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contract ct = new Contract();
        ct.AccountId= acc.Id;
        ct.Status='Draft';
        ct.ContractTerm = 20;
        insert ct;
        
        Clause__c cl1 = new Clause__c();
        cl1.Name = 'test1';
        cl1.Description__c = 'test description';
        cl1.Type__c = 'Financial'; 
       
        Clause__c cl2 = new Clause__c();
        cl2.Name = 'test2';
        cl2.Description__c = 'test description';
        cl2.Type__c = 'Other';
        
        insert cl1;
        insert cl2;
        
        Contract_Clause__c ccl = new Contract_Clause__c();
        ccl.Clause__c = cl1.Id;
        ccl.Contract__c = ct.Id;
        
        Contract_Clause__c ccl1 = new Contract_Clause__c();
        ccl1.Clause__c = cl2.Id;
        ccl1.Contract__c = ct.Id;
        
        insert ccl;
        insert ccl1;
        
        List<Contract_Clause__c> clist = new List<Contract_Clause__c>();
        clist = RemoveClausesController.getRelatedClauses(ct.Id);
        system.assertEquals(2, clist.size());
        
        List<String> tempCL = new List<String>();
        tempCL.add(ccl1.Id);
        
        
         List<Contract_Clause__c> clist1 = new List<Contract_Clause__c>();
        try{
           
            RemoveClausesController.removeClausesFromContract(tempCL);
            clist1 = RemoveClausesController.getRelatedClauses(ct.Id);
            
        }catch(DmlException e){
  			
        }
         test.stopTest();
        system.assertEquals(1, clist1.size());
        
        
    }
    
    
    
}
public class ContractRelatedListController {
    
    @auraEnabled
    public static List<Clause__c> getAllClauses()
    {
        List<Clause__c> clauseList = new List<Clause__c>();

            for(Clause__c cl: [select name, createdById, Description__c, lastModifiedById, OwnerId, Type__c FROM Clause__c])
            {
                clauseList.add(cl);
            }
            return clauseList;

    }
    
    @auraEnabled
    public static List<Contract_Clause__c> getRelatedClauses(ID recordid)
    {
     
            List<Contract_Clause__c> ccList = [select id, name,createdById,LastModifiedById,Clause__c,Contract__c FROM Contract_Clause__c WHERE Contract__c=:recordid];
        	return ccList;   
     
    }
}
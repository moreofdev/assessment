public class CustomContractUIController {
    
	@auraEnabled
    public static List<Contract> getContracts(ID recordid)
    {
         List<Contract> contractList = new List<Contract>();
       
            contractList = [select Id, AccountId, ContractTerm, Status, Description, ContractNumber from Contract WHERE ID=:recordid];

		return contractList;       
    }
    
    
}
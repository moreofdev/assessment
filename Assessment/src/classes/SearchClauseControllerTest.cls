@isTest
public class SearchClauseControllerTest {
    
    
    public static testMethod void getSearchResults(){
        
        List<Clause__c> l1 = new List<Clause__c>();
        List<Clause__c> l2 = new List<Clause__c>();
        
        for(integer i=0;i<10;i++)
        {
            Clause__c cl = new Clause__c();
            cl.Name = 'test'+i;
            cl.Description__c = 'test description'+i;
            cl.Type__c = 'Financial';
            l1.add(cl);
        }
        for(integer i=0;i<5;i++)
        {    
            Clause__c cl1 = new Clause__c();
            cl1.Name = 'searchText'+i;
            cl1.Description__c = 'test description'+i;
            cl1.Type__c = 'Other';
            l2.add(cl1);
        }
        test.startTest();
        insert(l1);
        insert(l2);
        
        String searchTerm = 'searchText';
        List<Clause__c> clauseList = SearchClauseController.getSearchResults(SearchTerm);

        String searchTerm1 = 'test';
        List<Clause__c> clauseList1 = SearchClauseController.getSearchResults(SearchTerm1);
        test.stopTest();
        
        system.assertEquals(5, clauseList.size());
        system.assertEquals(10, clauseList1.size());
        
    }
    
    public static testmethod void createParentRecords(){

       	List<Clause__c> clauselist = new List<Clause__c>();
        for(integer i=0;i<10;i++)
        {
            Clause__c cl = new Clause__c();
            cl.Name = 'test'+i;
            cl.Description__c = 'test description'+i;
            cl.Type__c = 'Financial';
             
        
           clauselist.add(cl);
           
        }
         test.startTest();
         insert(clauselist);
      	 test.stopTest();
        
          List<String> cIds = new List<String>();
         for(Clause__c clause: clauselist)
        {
           cIds.add(clause.Id);    
        }
       
      
      	test.startTest();
        Account acc = new Account();
        acc.Name = 'Test Account';
        insert acc;
        
        Contract ct = new Contract();
        ct.AccountId= acc.Id;
        ct.Status='Draft';
        ct.ContractTerm = 20;
        insert ct;
        
        
        SearchClauseController.createParentRecords(ct.Id, cIds);
        
         List<Contract_Clause__c> ccList = [select id FROM Contract_Clause__c WHERE Contract__c=:ct.Id];
         test.stopTest();
        
        system.assertEquals(10, ccList.size());
        
        //*****************************************below is for negative test***********************************************//
       
        List<Clause__c> tempClauseList = new List<Clause__c>();
        for(integer i=0;i<2;i++)
        {
            Clause__c cl = new Clause__c();
            cl.Name = 'test'+i;
            cl.Description__c = 'test description'+i;
            cl.Type__c = 'Financial';
            
           tempClauseList.add(cl);
           
        }
        insert tempClauseList;
        
         List<String> clauseIds = new List<String>();
         for(Clause__c clauses: tempClauseList)
        {
           clauseIds.add(clauses.Id);    
        }
       
        
        Account ac = new Account();
        ac.Name = 'Test Account';
        insert ac;
        
        Contract c = new Contract();
        c.AccountId= ac.Id;
        c.Status='test';
        c.ContractTerm = 20;
      
        String contractId= c.id;
        try{
            insert c;
            SearchClauseController.createParentRecords(contractId, clauseIds);
        }catch(DmlException e){
		
           Boolean bool = false;
            if(e.getDmlFields(0) != null){
                bool = true;
            }
            System.assert(bool);
            
          
        }
        
    }
   
    
    
}
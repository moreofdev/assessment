@isTest
public class CustomContractUIControllerTest {

    //methods to test id a contract is returned to display its details
     public static testMethod void getContractList()
    {
           test.startTest();
        Account acc = new Account();
        acc.Name = 'Test Account';
       	insert acc;
        
        Contract ct = new Contract();
        ct.AccountId= acc.Id;
        ct.Status='Draft';
        ct.ContractTerm = 15;
     
        
        insert ct;
       
        List<Contract>  ctr = CustomContractUIController.getContracts(ct.Id);
       test.stopTest();
        
        System.assertEquals( ctr.size(),1);
        System.assert( ctr  != null);
        
        
    }
}
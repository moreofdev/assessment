public class RemoveClausesController {
    @auraEnabled
    public static List<Contract_Clause__c> getRelatedClauses(ID recordid)
    {
        List<Contract_Clause__c> ccList  = new List<Contract_Clause__c>();
            ccList   = [select id, name,createdById,LastModifiedById,Clause__c,Contract__c FROM Contract_Clause__c WHERE Contract__c=:recordid];
            return ccList;              
        
    }
    @auraEnabled
    public static void removeClausesFromContract(List<String> clauseList)
    {
       
        List<Contract_Clause__c> recToRemove = [select Id from Contract_Clause__c Where id IN : clauseList];
         
        try{
            if( ! recToRemove.isEmpty())
            { 
                 delete recToRemove;
            }
           
        }
        catch(DmlException e){
            system.debug('error occured while removing a clause'+ e.getMessage());
        }
    }
}
public class SearchClauseController {
    
    @auraEnabled
    public static List<Clause__c> getSearchResults(String SearchTerm)
    {
        String st = '%'+SearchTerm+'%';        
        List<Clause__c> clauseList = new List<Clause__c>();
      
            //all the clauses with the search term
            for(Clause__c cl: [select id, name, createdById, Description__c, lastModifiedById, OwnerId, Type__c FROM Clause__c WHERE name like:st ])
            {    
                clauseList.add(cl);  
            }      
            
           return clauseList;
    
        
    }
    
    @auraEnabled
    public static void createParentRecords(String parentId, List<String> listOfClauseIds){
        
        List<Contract_Clause__c> ccList = new List<Contract_Clause__c>();
     
        for(string cid : listOfClauseIds )
        {
            Contract_Clause__c cc = new Contract_Clause__c();
            system.debug('contract id= '+parentId);
            system.debug('clause id= '+cid);
            cc.Clause__c = cid;
            cc.Contract__c = parentId;
            ccList.add(cc);
        }
        try{
            if(! ccList.isEmpty())
            {
                 insert ccList;
            }
 
        }catch(DMLException e){
           system.debug('error occured'+e.getMessage());
        }
        
    }
    
}